<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>University of Texas</title>

    <!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/landing-page.css" rel="stylesheet">
    
<link href="css/styles2.css" rel="stylesheet">
<link rel="stylesheet" href="css/animate.css">

<link rel="stylesheet" type="text/css" href="./css/jquery.fullPage.css" />



    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">


<style>
html.fp-enabled {max-height:none!important;overflow-x:hidden;overflow-y:visible;}

@media (min-width:767px) {

.section {max-height:795px!important; } 

.section {background-color:none!important;}
.content {max-height:none!important;}
#section-nine {max-height:550px!important;}
}

#fp-nav ul li .fp-tooltip {color:#f06f23!important;}
#fp-nav ul li a span, .fp-slidesNav ul li a span {width:12px;height:12px;background:#fff; border:1px solid #f06f23;}
#fp-nav ul li, .fp-slidesNav ul li {height:20px!important;}
#fp-nav ul li a.active span, .fp-slidesNav ul li a.active span, #fp-nav ul li:hover a.active span, .fp-slidesNav ul li:hover a.active span {margin:-2px 0 0 -2px!important;background:#f06f23;}


</style>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    
    
 
   
<script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>

	<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script>

    <script src="js/bootstrap.min.js"></script>
    
    

<script>

$(window).scroll(function() {
if ($(document).scrollTop() > 150) {
$('.navbar').addClass('shrink');
$('.navbar-brand').show(0).delay(1000);
$('.navbar-right').show(0).delay(1000);
$('.navbar-brand').css('opacity', '1');
$('.navbar-right').css('opacity', '1');

}
else {
$('.navbar').removeClass('shrink');
$('.navbar-brand').hide();
$('.navbar-right').hide(); 
$('.navbar-brand').css('opacity', '0'); 
$('.navbar-right').css('opacity', '0');
}
});



</script>



<script src="./js/jquery.fullPage.min.js"></script>


<script src="./js/vendors/jquery.slimscroll.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-smooth-scroll/1.5.6/jquery.smooth-scroll.min.js"></script>



	<script type="text/javascript">
	
		$(document).ready(function() {
			
			if(screen.width > 767) { 

			
			$('#fullpage').fullpage({
				'verticalCentered': false,
				'css3': true,
				'scrollingSpeed': 1500,
				'navigation': true,
				'autoScrolling': false,
				'scrollBar':true,
				'fitToSection': false,
				'touchSensitivity': 15,
				
				'anchors': ['start', 'ways-to-save', 'retirement-needs', 'financial-basics', 'ut-options', 'stay-on-track', 'market-volatility', 'maximize-savings', 'plan-for-income'],
				'navigationPosition': 'right',
				'navigationTooltips': ['START', 'FIND WAYS TO SAVE', 'WHAT YOU NEED FOR RETIREMENT', 'FINANCIAL BASICS', 'YOUR UT OPTIONS', 'STAY ON TRACK', 'MARKET VOLATILITY', 'MAXIMIZE YOUR SAVINGS', 'PLAN FOR INCOME'],

			});
			
			$('#startbutton, .continue').click(function(){
    $.fn.fullpage.moveSectionDown();
});

$('.previous').click(function(){
    $.fn.fullpage.moveSectionUp();
});

$.fn.fullpage.setAllowScrolling(true);
$.fn.fullpage.setKeyboardScrolling(false);
$.fn.fullpage.setFitToSection(false);

			} else {
				
				 $(document).ready(function() {

      $('a').smoothScroll({offset: -75});
				 });
				 
			}
			

		});
	</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/skrollr/0.6.30/skrollr.min.js" type="text/javascript"></script>


<script>

if ( $(window).width() > 739) {    

        // Start Skrollr
        window.onload = function() {
			
			
            skrollr.init({
                forceHeight: false
            });
			
			
		}
        }
    </script>
    
<script>
$(document).ready(function() {
	$("#startbutton").hover(function(){
		$("#purplebutton, #orangebutton").removeClass("pulse");
	    $("#purplebutton").animate({ opacity: 0 }, 'slow');
	}, function() {
		$("#purplebutton, #orangebutton").addClass("pulse");
	    $("#purplebutton").animate({ opacity: 1 }, 'slow');
	});
});
</script>

</head>

<body>


   <!-- Navigation -->
    <nav class=" navbar navbar-default navbar-fixed-top topnav normal" role="navigation">
        <div class="container topnav">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="scroller navbar-brand topnav unshrunk" href="#"><img src="./img/ut-logo-white.png"/></a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right unshrunk">
                    <li>
                        <a class="btn-default btn btn-lg" href="http://www.utsystem.edu/offices/employee-benefits/enroll-retirement-manager" target="_blank">ENROLL!</a>
                    </li>
                    <li>
                       <a class="btn-default btn btn-lg" href="http://www.utretirement.utsystem.edu/providers.htm" target="_blank">FINANCIAL PRO</a>
                    </li>
                    
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>


<?php include 'desktop.php'; ?>







 


</body>

</html>
