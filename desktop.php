<div id="fullpage" class="page container">

	<div class="content">

            
		<div class="section" id="section-one">
       
			<section class="hidden-xs section-one" >
				<div class="row">

					<div class="path-left col-sm-6 col-md-6 col-lg-6">


						<img alt=""  id="sec1_img1" src="./img/pieces_intro.png"/>
						<img alt=""  id="sec1_img2" src="./img/img_sec1.png"/>
						<a id="startbutton" class=" scroller"><img alt=""  id="orangebutton" class="animate pulse"  src="./img/startorange.png"/><img alt=""  id="purplebutton" class="animate pulse"  src="./img/start-noarrow.png"/></a>

					
					</div>

					<div class="path-right col-sm-6 col-md-6 col-lg-6">
						<h1>Welcome to the University of Texas System Retirement Planning game board!</h1>
						<p>Saving for retirement is more than a game of chance. It takes planning and perseverance. Click on each stage of retirement planning from the menu at right to find educational tips and tools to help you stay in the game. You can also use your mouse to scroll through the stages. Make your first move today!</p>
						<a class="purplebutton btn-default btn btn-md" href="http://www.utsystem.edu/offices/employee-benefits/enroll-retirement-manager" target="_blank">ENROLL NOW!</a>
						<a class="purplebutton btn-default btn btn-md" href="http://www.utsystem.edu/offices/employee-benefits/approved-providers" target="_blank">MEET WITH A FINANCIAL ADVISOR</a>
					</div>

				</div>
			</section>

                  
			<section class="visible-xs section-one" >
				<div class="row">

					<div class="path-left col-xs-12">
						<img alt=""  id="utlogo" src="./img/ut-logo.png"/>
						<img alt=""  id="intropieces" src="./img/m-img_sec1.png"/>
						<a href="#section-two" id="startbutton" class="scroller"  ><img alt=""  id="orangebutton" class="animate pulse"  src="./img/startorange.png"/><img alt=""  id="purplebutton" class="animate pulse"  src="./img/start-noarrow.png"/></a>
					</div>

					<div class="path-right col-xs-12">
						<h1>Welcome to the University of Texas System Retirement Planning game board!</h1>
						<p>Saving for retirement is more than a game of chance. It takes planning and perseverance. Click on each stage of retirement planning from the menu at right to find educational tips and tools to help you stay in the game. You can also use your mouse to scroll through the stages. Make your first move today!</p>

						<span class="buttons">
							<a class="purplebutton btn-default btn btn-md" href="http://www.utsystem.edu/offices/employee-benefits/ut-retirement-program/enroll.htm" target="_blank">ENROLL NOW!</a>
							<a class="purplebutton btn-default btn btn-md" href="http://www.utsystem.edu/offices/employee-benefits/approved-providers" target="_blank">MEET WITH A FINANCIAL ADVISOR</a>
						</span>
					</div>

				</div>
			</section>
        
		</div>
             
             
    
    <div class="section" id="section-two">
                 
			<section class="hidden-xs section-two">
				<div class="row">

					<div class="path-left col-sm-6 col-md-6 col-lg-6">
						<table>
							<tr>
								<td><img alt=""  class="number" src="img/dot1.png" /></td>
								<td><h2>FIND WAYS TO SAVE</h2></td>
							</tr>
						</table>
						<p>No matter how far away the finish may seem, it's never too early to start saving! Even small moves can have a big impact on your future.</p>
						<table class="linklist">
							<tr>
								<td><img alt=""  src="img/icon_video.png" /></td>
								<td class="rcol"><a href="http://www.brainshark.com/fmr/vu?pi=zHfz13vrC2zKVjFz0" target="_blank">Debt and Budgeting</a></td>
							</tr>
							<tr>
								<td><img alt=""  src="img/icon_video.png" /></td>
								<td class="rcol"><a href="http://www.brainshark.com/lfg/vu?pi=zIAzgTlipzjZlz0" target="_blank">How to Lower Your Current Taxes</a></td>
							</tr>
							<tr>
								<td><img alt=""  src="img/icon_web.png" /></td>
								<td class="rcol"><a href="https://marketing.voyacdn.com/3016825/" target="_blank">What Expenses Can You Cut Back On</a></td>
							</tr>
							<tr>
								<td><img alt=""  src="img/icon_video.png" /></td>
								<td class="rcol"><a href="http://www.brainshark.com/lfg/vu%3Fpi=zGQz17u3N8zjZlz0" target="_blank">Wake Up and Smell the Coffee Video</a></td>
							</tr>
						</table>

						<div class="col-sm-12 col-md-12 hidden-lg" style="margin-left:-15px;">
							<h3>CONSIDERATIONS FOR GEN Y</h3>
							<table class="linklist">
								<tr>
									<td><img alt=""  src="img/icon_video.png" /></td>
									<td><a href="http://www.brainshark.com/lfg/vu?pi=zI2zgdXuZzjWuz0" target="_blank">Gen Y</a></td>
								</tr>
							</table>
						</div>
						<div class="col-sm-12 col-md-12 hidden-lg" style="margin-left:-15px;">
							<h3>CONSIDERATIONS FOR WOMEN</h3>
							<table class="linklist">
								<tr>
									<td><img alt=""  src="img/icon_video.png" /></td>
									<td><a href="https://www.brainshark.com/Voya/RetirementPlanningforWomenUT" target="_blank">Women and Investing</a></td>
								</tr>
								<tr>
									<td><img alt=""  src="img/icon_video.png" /></td>
									<td><a href="http://www.brainshark.com/lfg/vu?pi=zHgzpZSTlzjWuz0" target="_blank">Why Women Should Save for Retirement</a></td>
								</tr>
                                
                                <tr>
									<td><img alt=""  src="img/icon_video.png" /></td>
									<td><a href="http://www.brainshark.com/fmr/UTthrivegetorganized" target="_blank">Get Organized</a></td>
								</tr>
                                
                                
							</table>
						</div>

						<a class="continue btn-default btn btn-md scroller"><i class="fa fa-chevron-down fa-lg"></i> CONTINUE</a>
						<a class="previous btn-default btn btn-md scroller">PREVIOUS <i class="fa fa-chevron-up fa-lg"></i></a>
					</div>

					<div class="path-right col-sm-6 col-md-6 col-lg-6">
						<div class="first-sub hidden-xs hidden-sm hidden-md col-lg-6 col-lg-offset-6">
							<h3>CONSIDERATIONS FOR GEN Y</h3>
							<table class="linklist">
								<tr>
									<td><img alt=""  src="img/icon_video.png" /></td>
									<td><a href="http://www.brainshark.com/lfg/vu?pi=zI2zgdXuZzjWuz0" target="_blank">Gen Y</a></td>
								</tr>
							</table>
						</div>
						<img alt=""  class="FGimg" data-700-top="opacity:0;width:70px;top:265px;left:203px;" data-320-top="opacity:1;width:110px;margin-top:0px;top:285px;margin-right:0;left:193px;" src="./img/pieces_single.png"/>
						<div class="second-sub hidden-xs hidden-sm hidden-md col-lg-7 col-lg-offset-5">
							<h3>CONSIDERATIONS FOR WOMEN</h3>
							<table class="linklist">
								<tr>
									<td><img alt=""  src="img/icon_video.png" /></td>
									<td><a href="https://www.brainshark.com/Voya/RetirementPlanningforWomenUT" target="_blank">Women and Investing</a></td>
								</tr>
								<tr>
									<td><img alt=""  src="img/icon_video.png" /></td>
									<td><a href="http://www.brainshark.com/lfg/vu?pi=zHgzpZSTlzjWuz0" target="_blank">Why Women Should Save for Retirement</a></td>
								</tr>
                                
                                <tr>
									<td><img alt=""  src="img/icon_video.png" /></td>
									<td><a href="http://www.brainshark.com/fmr/UTthrivegetorganized" target="_blank">Get Organized</a></td>
								</tr>
                                
							</table>
						</div>
					</div>

				</div>
			</section>

                    
			<section class="visible-xs section-two" >
				<div class="row">

					<div class="path-left col-xs-12">
						<img alt=""  data-500-top="padding-top:0px; opacity:0;" data-450-top="padding-top:10px; opacity:1;" class="mobilepiece" src="./img/mobilepiece.png"/>
						<table>
							<tr>
								<td><img alt=""  class="number" src="img/dot1.png" /></td>
								<td><h2>FIND WAYS TO SAVE</h2></td>
							</tr>
						</table>
						<p>No matter how far away the finish may seem, it's never too early to start saving! Even small moves can have a big impact on your future.</p>
						<table class="linklist">
							<tr>
								<td><img alt=""  src="img/icon_video.png" /></td>
								<td><a href="http://www.brainshark.com/fmr/vu?pi=zHfz13vrC2zKVjFz0" target="_blank">Debt and Budgeting</a></td>
							</tr>
							<tr>
								<td><img alt=""  src="img/icon_video.png" /></td>
								<td><a href="http://www.brainshark.com/lfg/vu?pi=zIAzgTlipzjZlz0" target="_blank">How to Lower Your Current Taxes</a></td>
							</tr>
							<tr>
								<td><img alt=""  src="img/icon_web.png" /></td>
								<td><a href="https://marketing.voyacdn.com/3016825/" target="_blank">What Expenses Can You Cut Back On</a></td>
							</tr>
							<tr>
								<td><img alt=""  src="img/icon_video.png" /></td>
								<td><a href="http://www.brainshark.com/lfg/vu%3Fpi=zGQz17u3N8zjZlz0" target="_blank">Wake Up and Smell the Coffee Video</a></td>
							</tr>
						</table>

						<div class="first-sub col-xs-12">
							<h3>CONSIDERATIONS FOR GEN Y</h3>
							<table class="linklist">
								<tr>
									<td><img alt=""  src="img/icon_video.png" /></td>
									<td><a href="http://www.brainshark.com/lfg/vu?pi=zI2zgdXuZzjWuz0" target="_blank">Gen Y</a></td>
								</tr>
							</table>
						</div>

						<div class="second-sub col-xs-12">
							<h3>CONSIDERATIONS FOR WOMEN</h3>
							<table class="linklist">
								<tr>
									<td><img alt=""  src="img/icon_video.png" /></td>
									<td><a href="https://www.brainshark.com/Voya/RetirementPlanningforWomenUT" target="_blank">Women and Investing</a></td>
								</tr>
								<tr>
									<td><img alt=""  src="img/icon_video.png" /></td>
									<td><a href="http://www.brainshark.com/lfg/vu?pi=zHgzpZSTlzjWuz0" target="_blank">Why Women Should Save for Retirement</a></td>
								</tr>
							</table>
						</div> 

						<span class="nextprev">
							<a href="#section-three" class="btn-default btn btn-md scroller" ><i class="fa fa-chevron-down fa-lg"></i> CONTINUE</a>
							<a href="#section-one" class=" btn-default btn btn-md scroller">PREVIOUS <i class="fa fa-chevron-up fa-lg"></i></a>
						</span>

					</div>

				</div>
			</section>
  
    </div>
  
  
		<div class="section" id="section-three">

			<section class="hidden-xs section-three">
				<div class="row">

					<div class="path-left col-sm-6 col-md-6 col-lg-6">
						<img alt="" id="sec3_img1" data-700-top="opacity:0;width:70px;top:254px;left:205px;" data-550-top="opacity:1;width:110px;margin-top:0px;top:250px;margin-right:0;left:195px;" src="./img/pieces_single.png" class="skrollable skrollable-after" style="opacity: 1; width: 110px; top: 250px; left: 195px; margin-top: 0px; margin-right: 0px;">
                    
                    
						<img alt=""  id="sec3_img2" data-800-top="padding-top:0px; opacity:0;left:-100px;" data-450-top="padding-top:10px; opacity:1;left:10px;" src="./img/img_sec3.png"/>
					</div>

					<div class="path-right col-sm-6 col-md-6 col-lg-6">
						<table>
							<tr>
								<td><img alt=""  class="number" src="img/dot2.png" /></td>
								<td><h2>CALCULATE WHAT YOU NEED FOR RETIREMENT</h2></td>
							</tr>
							</table>
							<p>In real life, a roll of the dice won't get you successfully to retirement. Use My Retirement Outlook to calculate what you will need.</p>
							<table class="linklist">
								<tr>
									<td><img alt=""  src="img/icon_calc.png" /></td>
									<td><a href="https://emro.voyaplans.com/emro/mro.action?mrwUTexas" target="_blank">How Much is Enough</a></td>
								</tr>
						</table>
						<a class="continue btn-default btn btn-md scroller"><i class="fa fa-chevron-down fa-lg"></i> CONTINUE</a>
						<a class="previous btn-default btn btn-md scroller">PREVIOUS <i class="fa fa-chevron-up fa-lg"></i></a>
					</div>

				</div>
			</section>


			<section class="visible-xs section-three">
				<div class="row">

					<div class="path-right col-xs-12">
						<img alt=""  data-500-top="padding-top:0px; opacity:0;" data-450-top="padding-top:10px; opacity:1;" class="mobilepiece" src="./img/m-img_sec3.png"/>
						<table>
							<tr>
								<td><img alt=""  class="number" src="img/dot2.png" /></td>
								<td><h2>CALCULATE WHAT YOU NEED FOR RETIREMENT</h2></td>
							</tr>
							</table>
							<p>In real life, a roll of the dice won't get you successfully to retirement. Use My Retirement Outlook to calculate what you will need.</p>
							<table class="linklist">
							<tr>
								<td><img alt=""  src="img/icon_calc.png" /></td>
								<td><a href="https://emro.voyaplans.com/emro/mro.action?mrwUTexas" target="_blank">How Much is Enough</a></td>
							</tr>
						</table>

						<span class="nextprev">
							<a href="#section-four" class="continue btn-default btn btn-md scroller"><i class="fa fa-chevron-down fa-lg"></i> CONTINUE</a>
							<a href="#section-two" class="previous btn-default btn btn-md scroller">PREVIOUS <i class="fa fa-chevron-up fa-lg"></i></a>
						</span>
					</div>

				</div>
			</section>

		</div>     


		<div class="section" id="section-four">

			<section class="hidden-xs section-four">
				<div class="row">

					<div class="path-left col-sm-6 col-md-6 col-lg-6">
						<table>
							<tr>
								<td><img alt=""   class="number" src="img/dot3.png" /></td>
								<td><h2>UNDERSTAND FINANCIAL BASICS</h2></td>
							</tr>
						</table>
						<p>Navigating towards retirement can be difficult if you don't know the rules. We offer some basic tips to help you plan your moves.</p>
						<table class="linklist">
							<tr>
								<td><img alt=""  src="img/icon_video.png" /></td>
								<td><a href="http://www.brainshark.com/lfg/vu?pi=zH7zsjVFGzjZlz0" target="_blank">The Social Security Express</a></td>
							</tr>
						</table>
						<a class="continue btn-default btn btn-md scroller"><i class="fa fa-chevron-down fa-lg"></i> CONTINUE</a>
						<a class="previous btn-default btn btn-md scroller">PREVIOUS <i class="fa fa-chevron-up fa-lg"></i></a>
					</div>

					<div class="path-right col-sm-6 col-md-6 col-lg-6">
						<img alt=""  id="sec4_img1" 
                        
                        
             data-700-top="opacity:0;width:70px;top:321px;left:219px;" data-550-top="opacity:1;width:110px;margin-top:0px;top:341px;margin-right:0;left:209px;"           
                        
                        
                       src="./img/pieces_single.png"/>
						<img alt=""  id="sec4_img2" 
                        
                        data-800-top="padding-top:0px; opacity:0;left:332px;" data-450-top="padding-top:10px; opacity:1;left:232px;"
                        
                        src="./img/img_sec4.png"/>
					</div>

				</div>
			</section>


			<section class="visible-xs section-four">
				<div class="row">

					<div class="path-left col-xs-12">
						<img alt=""  data-500-top="padding-top:0px; opacity:0;" data-450-top="padding-top:10px; opacity:1;" class="mobilepiece" src="./img/m-img_sec4.png"/>
						<table>
							<tr>
								<td><img alt=""   class="number" src="img/dot3.png" /></td>
								<td><h2>UNDERSTAND FINANCIAL BASICS</h2></td>
							</tr>
						</table>
						<p>Navigating towards retirement can be difficult if you don't know the rules. We offer some basic tips to help you plan your moves.</p>
						<table class="linklist">
							<tr>
								<td><img alt=""  src="img/icon_video.png" /></td>
								<td><a href="http://www.brainshark.com/lfg/vu?pi=zH7zsjVFGzjZlz0" target="_blank">The Social Security Express</a></td>
							</tr>
						</table>

						<span class="nextprev">
							<a href="#section-five" class="continue btn-default btn btn-md scroller"><i class="fa fa-chevron-down fa-lg"></i> CONTINUE</a>
							<a href="#section-three" class="previous btn-default btn btn-md scroller">PREVIOUS <i class="fa fa-chevron-up fa-lg"></i></a>
						</span>
					</div>

				</div>
			</section>

		</div>

	
		<div class="section" id="section-five">

			<section class="hidden-xs section-five">
				<div class="row">

					<div class="path-left col-sm-6 col-md-6 col-lg-6">
						<img alt=""  id="sec5_img2"
                       
                        data-800-top="padding-top:0px; opacity:0;left:-200px;" data-550-top="padding-top:10px; opacity:1;left:87px;"
                        
                       src="./img/img_sec5.png"/>
						<img alt=""  id="sec5_img1" 
                       
                         
                        data-700-top="opacity:0;width:70px;top:361px;left:320px;" data-550-top="opacity:1;width:110px;margin-top:0px;top:381px;margin-right:0;left:310px;"
                        
                        
                       src="./img/pieces_single.png"/>
					</div>

					<div class="path-right col-sm-6 col-md-6 col-lg-6">
					<table>
						<tr>
							<td><img alt=""   class="number" src="img/dot4.png" /></td>
							<td><h2>CHECK OUT YOUR UT OPTIONS</h2></td>
						</tr>
					</table>
					<p>UT offers employees a number of options to help as you advance towards retirement. Check out these beneficial resources.</p>
					<table class="linklist">
						<tr>
							<td><img alt=""  src="img/icon_video.png" /></td>
							<td><a href="http://www.brainshark.com/valic/vu?pi=zCgzCdbt5z0z0" target="_blank">New Retirement Manager Online</a></td>
						</tr>
                        
                        <tr>
							<td><img alt=""  src="img/icon_calc.png" /></td>
							<td><a href="https://emro.voyaplans.com/emro/mro.action?mrwUTexas" target="_blank">Calculate Your Outlook Here!</a></td>
						</tr>
                        
                        
                        
						<tr>
							<td><img alt=""  src="img/icon_video.png" /></td>
							<td><a href="https://www.youtube.com/watch?v=lrBiLAMEj_4&feature=youtu.be" target="_blank">Is TRS Enough?</a></td>
						</tr>
						<tr>
							<td><img alt=""  src="img/icon_video.png" /></td>
							<td><a href="http://www.brainshark.com/fmr/UnivofTexasGettingontheRightPa" target="_blank">Workplace Savings</a></td>
						</tr>
                        
                        <tr>
							<td><img alt=""  src="img/icon_web.png" /></td>
							<td><a href="http://www.utsystem.edu/offices/employee-benefits/federal-tax-savers-credit" target="_blank">The Saver's Tax Credit</a></td>
						</tr>
					</table>
					<a class="continue btn-default btn btn-md scroller"><i class="fa fa-chevron-down fa-lg"></i> CONTINUE</a>
					<a class="previous btn-default btn btn-md scroller">PREVIOUS <i class="fa fa-chevron-up fa-lg"></i></a>
					</div>

				</div>
			</section>


			<section class="visible-xs section-five">
				<div class="row">

					<div class="path-right col-xs-12">
						<img alt=""  data-500-top="padding-top:30px; opacity:0;" data-450-top="padding-top:10px; opacity:1;" class="mobilepiece" src="./img/m-img_sec5.png"/>
						<table>
							<tr>
								<td><img alt=""   class="number" src="img/dot4.png" /></td>
								<td><h2>CHECK OUT YOUR UT OPTIONS</h2></td>
							</tr>
						</table>
						<p>UT offers employees a number of options to help as you advance towards retirement. Check out these beneficial resources.</p>
						<table class="linklist">
							<tr>
								<td><img alt=""  src="img/icon_video.png" /></td>
								<td><a href="http://www.brainshark.com/valic/vu?pi=zCgzCdbt5z0z0" target="_blank">New Retirement Manager Online</a></td>
							</tr>
							<tr>
								<td><img alt=""  src="img/icon_ipod.png" /></td>
								<td><a href="http://www.utsystem.edu/offices/employee-benefits" target="_blank">Is TRS Enough?</a></td>
							</tr>
							<tr>
								<td><img alt=""  src="img/icon_video.png" /></td>
								<td><a href="http://www.brainshark.com/fmr/UnivofTexasGettingontheRightPa" target="_blank">Workplace Savings</a></td>
							</tr>
						</table>

						<span class="nextprev">
							<a href="#section-six" class="continue btn-default btn btn-md scroller"><i class="fa fa-chevron-down fa-lg"></i> CONTINUE</a>
							<a href="#section-four" class="previous btn-default btn btn-md scroller">PREVIOUS <i class="fa fa-chevron-up fa-lg"></i></a>
						</span>

					</div>

				</div>
			</section>

		</div>
            
          
		<div class="section" id="section-six">
			<section class="hidden-xs section-six">
				<div class="row">

					<div class="path-left col-sm-6 col-md-6 col-lg-6">
						<table>
							<tr>
								<td><img alt=""  class="number" src="img/dot5.png" /></td>
								<td><h2>STAY ON TRACK</h2></td>
							</tr>
						</table>
						<p>Staying in the game for retirement takes more than counting spaces. Understand the impact of using your retirement savings before you cross the finish.</p>
						<table class="linklist">
							<tr>
								<td><img alt=""  src="img/icon_video.png" /></td>
								<td><a href="http://www.brainshark.com/lfg/vu?pi=zI0ztqiVLzjWuz0" target="_blank">Understanding the Cost of a Loan</a></td>
							</tr>
							<tr>
								<td><img alt=""  src="img/icon_video.png" /></td>
								<td><a href="https://www.brainshark.com/Voya/AssetAllocationUT" target="_blank">Asset Allocation</a></td>
							</tr>
						</table>
						<a class="continue btn-default btn btn-md scroller"><i class="fa fa-chevron-down fa-lg"></i> CONTINUE</a>
						<a class="previous btn-default btn btn-md scroller">PREVIOUS <i class="fa fa-chevron-up fa-lg"></i></a>
					</div>

					<div class="path-right col-sm-6 col-md-6 col-lg-6"><img alt=""  id="sec6_img2" data-700-top="opacity:0;top:0px;" data-310-top="opacity:1;top:210px;" src="./img/img_sec6.png"/><img alt=""  id="sec6_img1"  data-600-top="opacity:0;width:70px;top:283px;left:184px;" data-550-top="opacity:1;width:110px;margin-top:0px;top:303px;margin-right:0;left:174px;"           
                         src="./img/pieces_single.png"/>
					</div>

				</div>
			</section>


			<section class="visible-xs section-six">
				<div class="row">

					<div class="path-left col-xs-12">
						<img alt=""  data-500-top="padding-top:30px; opacity:0;" data-450-top="padding-top:10px; opacity:1;" class="mobilepiece" src="./img/m-img_sec6.png"/>
						<table>
							<tr>
								<td><img alt=""   class="number" src="img/dot5.png" /></td>
								<td><h2>STAY ON TRACK</h2></td>
							</tr>
						</table>
						<p>Staying in the game for retirement takes more than counting spaces. Understand the impact of using your retirement savings before you cross the finish.</p>
						<table class="linklist">
							<tr>
								<td><img alt=""  src="img/icon_video.png" /></td>
								<td><a href="http://www.brainshark.com/lfg/vu?pi=zI0ztqiVLzjWuz0" target="_blank">Understanding the Cost of a Loan</a></td>
							</tr>
							<tr>
								<td><img alt=""  src="img/icon_video.png" /></td>
								<td><a href="https://www.brainshark.com/Voya/AssetAllocationUT" target="_blank">Asset Allocation</a></td>
							</tr>
						</table>

						<span class="nextprev">
							<a href="#section-seven" class="continue btn-default btn btn-md scroller"><i class="fa fa-chevron-down fa-lg"></i> CONTINUE</a>
							<a href="#section-five" class="previous btn-default btn btn-md scroller">PREVIOUS <i class="fa fa-chevron-up fa-lg"></i></a>
						</span>

					</div>

				</div>
			</section>

		</div>

        
		<div class="section" id="section-seven">

			<section class="hidden-xs section-seven">
				<div class="row">

					<div class="path-left col-sm-6 col-md-6 col-lg-6">
						<div class="first-sub">
							<h3>JOB CHANGE</h3>
							<table class="linklist">
								<tr>
									<td><img alt=""  src="img/icon_video.png" /></td>
									<td><a href="https://www.brainshark.com/Voya/PositiveChangeUT" target="_blank">Job Loss Seminar</a></td>
								</tr>
							</table>
						</div>
						<img alt=""  id="sec7_img1"   
                        data-700-top="opacity:0;width:70px;top:235px;left:270px;" data-550-top="opacity:1;width:110px;margin-top:0px;top:255px;margin-right:0;left:260px;" src="./img/pieces_single.png"/>
						<img alt=""  id="sec7_img2" data-700-top="opacity:0.8;transform:rotate(-30deg);" data-400-top="opacity:1;transform:rotate(10deg);"   src="./img/img_sec7.png"/>
					</div>

					<div class="path-right col-sm-6 col-md-6 col-lg-6">
						<table>
							<tr>
								<td><img alt=""  class="number" src="img/dot6.png" /></td>
								<td><h2>MANAGE MARKET VOLATILITY</h2></td>
							</tr>
						</table>
						<p>Investing your retirement savings can feel like a game of chance. Learn how to make the right moves to keep your funds growing.</p>
						<table class="linklist">
							<tr>
								<td><img alt=""  src="img/icon_video.png" /></td>
								<td><a href="https://www.brainshark.com/Voya/StayingtheCourseUT" target="_blank">Staying the Course</a></td>
							</tr>
						</table>
						<a class="continue btn-default btn btn-md scroller"><i class="fa fa-chevron-down fa-lg"></i> CONTINUE</a>
						<a class="previous btn-default btn btn-md scroller">PREVIOUS <i class="fa fa-chevron-up fa-lg"></i></a>
					</div>

				</div>
			</section>


			<section class="visible-xs section-seven">
				<div class="row">

					<div class="path-right col-xs-12">
						<img alt=""  data-500-top="padding-top:0px; opacity:0;"	data-450-top="padding-top:10px; opacity:1;" class="mobilepiece" src="./img/m-img_sec7.png"/>
						<table>
							<tr>
								<td><img alt=""   class="number" src="img/dot6.png" /></td>
								<td><h2>MANAGE MARKET VOLATILITY</h2></td>
							</tr>
						</table>
						<p>Investing your retirement savings can feel like a game of chance. Learn how to make the right moves to keep your funds growing.</p>			
						<table class="linklist">
							<tr>
								<td><img alt=""  src="img/icon_video.png" /></td>
								<td><a href="https://www.brainshark.com/Voya/StayingtheCourseUT" target="_blank">Staying the Course</a></td>
							</tr>
						</table>
						<h3>JOB CHANGE</h3>
						<table class="linklist">
							<tr>
								<td><img alt=""  src="img/icon_video.png" /></td>
								<td><a href="https://www.brainshark.com/Voya/PositiveChangeUT" target="_blank">Job Loss Seminar</a></td>
							</tr>
						</table>

						<span class="nextprev">
							<a href="#section-eight" class="continue btn-default btn btn-md scroller"><i class="fa fa-chevron-down fa-lg"></i> CONTINUE</a>
							<a href="#section-six" class="previous btn-default btn btn-md scroller">PREVIOUS <i class="fa fa-chevron-up fa-lg"></i></a>
						</span>

					</div>

				</div>
			</section>

		</div>


		<div class="section" id="section-eight">

			<section class="hidden-xs section-eight">
				<div class="row">

					<div class="path-left col-sm-6 col-md-6 col-lg-6">
						<table>
							<tr>
								<td><img alt=""  class="number" src="img/dot7.png" /></td>
								<td><h2>MAXIMIZE YOUR SAVINGS</h2></td>
							</tr>
						</table>
						<p>As you advance through life milestones, it's likely that you'll have savings spread all across the board. Make a plan to keep your savings moving in the right direction.</p>
						<table class="linklist">
							<tr>
								<td><img alt=""  src="img/icon_video.png" /></td>
								<td><a href="https://www.brainshark.com/Voya/RetirementPlanRolloverOptionUT" target="_blank">Rollover Concepts</a></td>
							</tr>
						</table>
						<a class="continue btn-default btn btn-md scroller"><i class="fa fa-chevron-down fa-lg"></i> CONTINUE</a>
						<a class="previous btn-default btn btn-md scroller">PREVIOUS <i class="fa fa-chevron-up fa-lg"></i></a>
					</div>

					<div class="path-right col-sm-6 col-md-6 col-lg-6"><img alt=""  id="sec8_img1" data-600-top="opacity:0;width:70px;top:180px;left:184px;" data-550-top="opacity:1;width:110px;margin-top:0px;top:211px;margin-right:0;left:174px;"   
                         src="./img/pieces_single.png"/>
					<img alt=""  id="sec8_img2" data-800-top="opacity:0;left:355px;" data-310-top="opacity:1;left:247px;" src="./img/img_sec8.png"/></div>

				</div>
			</section>


			<section class="visible-xs section-eight">
				<div class="row">

					<div class="path-left col-xs-12">
						<img alt=""  data-500-top="padding-top:30px; opacity:0;" data-450-top="padding-top:10px; opacity:1;" class="mobilepiece" src="./img/m-img_sec8.png"/>
						<table>
							<tr>
								<td><img alt=""   class="number" src="img/dot7.png" /></td>
								<td><h2>MAXIMIZE YOUR SAVINGS</h2></td>
							</tr>
						</table>
						<p>As you advance through life milestones, it's likely that you'll have savings spread all across the board. Make a plan to keep your savings moving in the right direction.</p>
						<table class="linklist">
							<tr>
								<td><img alt=""  src="img/icon_video.png" /></td>
								<td><a href="https://www.brainshark.com/Voya/RetirementPlanRolloverOptionUT" target="_blank">Rollover Concepts</a></td>
							</tr>
						</table>

						<span class="nextprev">
							<a href="#section-nine" class="continue btn-default btn btn-md scroller"><i class="fa fa-chevron-down fa-lg"></i> CONTINUE</a>
							<a href="#section-seven" class="previous btn-default btn btn-md scroller">PREVIOUS <i class="fa fa-chevron-up fa-lg"></i></a>
						</span>
					</div>

				</div>
			</section>

		</div>
 
            
		<div class="section" id="section-nine">

			<section class="hidden-xs section-nine">
				<div class="row">

					<div class="path-left col-sm-6 col-md-6 col-lg-6">
						<img alt=""  id="sec9_img2" data-700-top="opacity:0;top:-207px;left:147px;" data-300-top="opacity:1;margin-top:0px;top:7px;margin-right:0;left:147px;"
                        
                         src="./img/img_sec9.png"/>
						<img alt=""  id="sec9_img1" data-700-top="opacity:0;width:70px;top:127px;left:216px;" data-550-top="opacity:1;width:110px;margin-top:0px;top:147px;margin-right:0;left:206px;" src="./img/pieces_single.png"/>
					</div>

					<div class="path-right col-sm-6 col-md-6 col-lg-6">
						<table>
							<tr>
								<td><img alt=""  class="number" src="img/dot8.png" /></td>
								<td><h2>PLAN FOR INCOME</h2></td>
							</tr>
						</table>
						<p>You've reached the finish line... Now what?  It's important to strategize how you will utilize your savings once retirement is achieved.</p>
						<table class="linklist">
							<tr>
								<td><img alt=""  src="img/icon_video.png" /></td>
								<td><a href="https://www.brainshark.com/Voya/CrackingtheNestEggUT" target="_blank">Cracking the Nest Egg</a></td>
							</tr>
						</table>
						<a href="#maximize-savings" class="previous btn-default btn btn-md scroller"><i class="fa fa-chevron-up fa-lg" style="padding:0 5px 0 0;"></i> PREVIOUS</a>
						<a class="btn-default btn btn-md scroller" href="#start">BACK TO TOP <i class="fa fa-angle-double-up fa-lg"></i></a>
					</div>

				</div>
			</section>

			<section class="visible-xs section-nine">
				<div class="row">

					<div class="path-right col-xs-12">
						<img alt=""  data-500-top="padding-top:30px; opacity:0;" data-450-top="padding-top:10px; opacity:1;" class="mobilepiece" src="./img/m-img_sec9.png"/>
						<table>
							<tr>
								<td><img alt=""   class="number" src="img/dot8.png" /></td>
								<td><h2>PLAN FOR INCOME</h2></td>
							</tr>
						</table>
						<p>You've reached the finish line... Now what?  It's important to strategize how you will utilize your savings once retirement is achieved.</p>
						<table class="linklist">
							<tr>
								<td><img alt=""  src="img/icon_video.png" /></td>
								<td><a href="https://www.brainshark.com/Voya/CrackingtheNestEggUT" target="_blank">Cracking the Nest egg</a></td>
							</tr>
						</table>

						<span class="nextprev">
							<a href="#section-eight" class="btn-default btn btn-md scroller"><i class="fa fa-chevron-up fa-lg" style="padding:0 5px 0 0;"></i> PREVIOUS</a>
							<a class="btn-default btn btn-md scroller" href="#start">BACK TO TOP <i class="fa fa-angle-double-up fa-lg"></i></a>
						</span>
					</div>
				
				</div>
			</section>

		</div>     

            
		<div class="footer">
			<section>
				<div class="row">
					<div class="footer-left col-xs-12 col-sm-6 col-md-6 col-lg-6"><img alt=""  src="./img/footer-left.jpg"/></div>
					<div class="footer-right col-xs-12 col-sm-6 col-md-6 col-lg-6">© 2015 The University of Texas System.<br/>
					601 Colorado Street, Austin, Texas 78701-2982. (512)499-4200</div>
				</div>
			</section>
		</div>

	</div>

</div> 